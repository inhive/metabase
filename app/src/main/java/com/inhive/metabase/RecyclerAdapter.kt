package com.inhive.metabase

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso


class RecyclerAdapter constructor(titles: MutableList<String>, releaseDate: MutableList<String>, rating: MutableList<String>, poster: MutableList<String>, id: MutableList<Int> ): RecyclerView.Adapter<RecyclerAdapter.ViewHolder> () {

    private val itemTitles: MutableList<String> = titles
    private val itemReleaseDates: MutableList<String> = releaseDate
    private val itemRatings: MutableList<String> = rating
    private val itemImageURLs: MutableList<String> = poster
    private val itemIds: MutableList<Int> = id


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_layout,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemTitle.text = itemTitles[position]
        holder.itemReleaseDate.text = itemReleaseDates[position]
        holder.itemRating.text = itemRatings[position]
        Picasso.get().load(itemImageURLs[position]).into(holder.itemImage)
        holder.itemId = itemIds[position].toString()
    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }

    inner class ViewHolder(itemView:View, var itemId: String? = null):RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView = itemView.findViewById(R.id.item_image)
        var itemTitle: TextView = itemView.findViewById(R.id.item_title)
        var itemReleaseDate: TextView = itemView.findViewById(R.id.item_releaseDate)
        var itemRating: TextView = itemView.findViewById(R.id.item_rating)

        init {
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, PreviewActivity::class.java)
                intent.putExtra("itemID", itemId)
                itemView.context.startActivity(intent)
            }
        }
    }
}