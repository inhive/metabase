package com.inhive.metabase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import com.squareup.picasso.Picasso

class HomeActivity : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //Navigation buttons
        val movieBtn: Button = findViewById(R.id.buttonMovies)
        val seriesBtn: Button = findViewById(R.id.buttonSeries)
        val musicBtn : Button = findViewById(R.id.buttonMusic)
        val gamesBtn : Button = findViewById(R.id.buttonGames)

        //Sub section buttons
        val helpBtn: ImageButton = findViewById(R.id.imageButtonHelp)
        val contactBtn: ImageButton = findViewById(R.id.imageButtonContact)
        val settingsBtn: ImageButton = findViewById(R.id.imageButtonSettings)

        //Recently watched movie picture
        val imageUrl: String = "https://image.tmdb.org/t/p/original/vVpEOvdxVBP2aV166j5Xlvb5Cdc.jpg"
        val homescreenImage : ImageView = findViewById(R.id.imageHomescreen)
        Picasso.get().load(imageUrl).into(homescreenImage)

        //Main section navigation
        movieBtn.setOnClickListener {
            val intent = Intent(this, MovieActivity::class.java)
            startActivity(intent)
        }

        seriesBtn.setOnClickListener {
            val intent = Intent(this, SeriesActivity::class.java)
            startActivity(intent)
        }

        musicBtn.setOnClickListener {
            val intent = Intent(this, MusicActivity::class.java)
            startActivity(intent)
        }

        gamesBtn.setOnClickListener {
            val intent = Intent(this, GamesActivity::class.java)
            startActivity(intent)
        }

        //Search section
        //TBD

        //Sub sections navigation
        helpBtn.setOnClickListener {
            val intent = Intent(this, HelpActivity::class.java)
            startActivity(intent)
        }

        settingsBtn.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        contactBtn.setOnClickListener {
            val intent = Intent(this, ContactActivity::class.java)
            startActivity(intent)
        }
    }
}