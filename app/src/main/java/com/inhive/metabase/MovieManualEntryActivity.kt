package com.inhive.metabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MovieManualEntryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_manual_entry)

        val saveBtn: Button = findViewById(R.id.buttonMovieAddManuallySave)

        saveBtn.setOnClickListener {
            //TODO: open the preview with filled informations
        }
    }
}