package com.inhive.metabase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MovieActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        val searchDBBtn: Button = findViewById(R.id.buttonSearchDB)
        val addMovieManuallyBtn: Button = findViewById(R.id.buttonMovieAddManually)

        searchDBBtn.setOnClickListener {
            val intent = Intent(this, SearchDBActivity::class.java)
            startActivity(intent)
        }

        addMovieManuallyBtn.setOnClickListener {
            val intent = Intent(this, MovieManualEntryActivity::class.java)
            startActivity(intent)
        }
    }
}