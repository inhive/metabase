package com.inhive.metabase

import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import org.json.JSONObject
import java.net.URL

class ApiHandler: AppCompatActivity() {

    val apiKey: String = "api_key=3fec234a19280c85911ee86c1098689f"
    val apiRequest: String = sentHttpGetRequest("titanic")
    val jsonObj = JSONObject(apiRequest)

    val textView: TextView = findViewById(R.id.appName)

    fun sentHttpGetRequest(query: String): String {
        val basePage: String = "https://api.themoviedb.org/3/"
        return URL(basePage + "search/movies?" + apiKey + "&query=" + query).readText()
    }

    fun changeTextView() {
        textView.text = "text you want to display"
    }
}