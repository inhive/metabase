package com.inhive.metabase

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

open class MainActivity : AppCompatActivity() {
    private final val prefNameFirstStart: String = "firstAppStart"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // uncomment this to reset the database / get a fresh database
        // HINT: mark the lines of Code and press ALT + C and then ALT + B to comment/uncomment
        /*deleteDatabase("metaBaseDatabase")
        val preferences: SharedPreferences = getSharedPreferences(prefNameFirstStart, MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putBoolean(prefNameFirstStart, true)
        editor.commit()*/
        //TODO: Implement a button in settings to reset the database (EGT-86)

        if (firstAppStart()){
            createDatabase()
        }

        val home = Intent(this, HomeActivity::class.java)
        startActivity(home)
    }

    private fun firstAppStart(): Boolean{
        val preferences: SharedPreferences = getSharedPreferences(prefNameFirstStart, MODE_PRIVATE)
        return if (preferences.getBoolean(prefNameFirstStart, true)) {
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putBoolean(prefNameFirstStart, false)
            editor.commit()
            true
        } else {
            false
        }
    }

    private fun createDatabase() {
        val databaseName = "metaBaseDatabase"
        val tableMovies = "movies"
        val tableSeries = "series"
        val tableMusic = "music"
        val tableGames = "games"

        val metaBaseDatabase = openOrCreateDatabase(databaseName, MODE_PRIVATE, null)
        metaBaseDatabase.execSQL("CREATE TABLE $tableMovies (movieID INTEGER, rating FLOAT, counter INTEGER)")
        metaBaseDatabase.execSQL("CREATE TABLE $tableSeries (movieID INTEGER, rating FLOAT, counter INTEGER)")
        metaBaseDatabase.execSQL("CREATE TABLE $tableMusic (movieID INTEGER, rating FLOAT, counter INTEGER)")
        metaBaseDatabase.execSQL("CREATE TABLE $tableGames (movieID INTEGER, rating FLOAT, counter INTEGER)")
        metaBaseDatabase.close()
    }
}