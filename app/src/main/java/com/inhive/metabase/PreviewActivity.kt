package com.inhive.metabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import org.json.JSONException
import android.widget.*

class PreviewActivity : AppCompatActivity() {
    private var queue: RequestQueue? = null
    private var itemId:String = ""
    private var url: String = "https://api.themoviedb.org/3/movie/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)

        val watchedBtn: Button = findViewById(R.id.preview_watched)
        val counterTextView: TextView = findViewById(R.id.item_watch_counter)
        val itemRatingBar: RatingBar = findViewById(R.id.preview_item_rating)

        var rating: Float
        var counter = 0
        val databaseName = "metaBaseDatabase"
        val tableName = "Movies"
        val metaBaseDatabase = openOrCreateDatabase(databaseName, MODE_PRIVATE, null)

        val id = intent.getStringExtra("itemID")
        url += id

        if (id != null) {
            itemId = id
        }
        queue = Volley.newRequestQueue(this)
        this.jsonParse()

        val cursor = metaBaseDatabase.rawQuery("SELECT * FROM $tableName WHERE movieID = '$id'", null)
        if (cursor.count == 1){
            cursor.moveToFirst()
            counter = cursor.getInt(2)
            counterTextView.text = counter.toString()
            rating = cursor.getFloat(1)
            itemRatingBar.rating = rating
            if (counter == 0) {
                watchedBtn.text = "Watched"
            }
            else {
                watchedBtn.text = "Watched again"
            }
        }
        metaBaseDatabase.close()

        watchedBtn.setOnClickListener {
            val metaBaseDatabase = openOrCreateDatabase(databaseName, MODE_PRIVATE, null)
            when (watchedBtn.text) {
                "Watched" -> {
                    counter += 1
                    watchedBtn.text = "Watched again"
                    counterTextView.text = counter.toString()
                }
                "Watched again" -> {
                    counter += 1
                    counterTextView.text = counter.toString()
                }
                else -> {
                    metaBaseDatabase.execSQL("INSERT INTO $tableName VALUES($id, '0', '0')")
                    watchedBtn.text = "Watched"
                }
            }

            metaBaseDatabase.execSQL("UPDATE $tableName SET counter = $counter WHERE movieID = $id")
            metaBaseDatabase.close()
        }

        itemRatingBar.setOnRatingBarChangeListener { p0, p1, p2 -> // todo: what the F is p0 & p2
            val metaBaseDatabase = openOrCreateDatabase(databaseName, MODE_PRIVATE, null)
            metaBaseDatabase.execSQL("UPDATE $tableName SET rating = $p1 WHERE movieID = $id")
            metaBaseDatabase.close()
        }
    }

    private val apiKey: String = "?api_key=3fec234a19280c85911ee86c1098689f"
    private var imageUrl: String = "http://image.tmdb.org/t/p/original/"

    private fun jsonParse() {
        val request =
            JsonObjectRequest(Request.Method.GET, url + apiKey, null, { response ->
                try {
                    val requestCredits =
                        JsonObjectRequest(Request.Method.GET, "$url/credits$apiKey", null, { responseCredits ->
                            try {
                                val itemImage: ImageView = findViewById(R.id.preview_item_image)
                                val itemTitle: TextView = findViewById(R.id.item_title)
                                val itemInfo: TextView = findViewById(R.id.item_info)
                                val itemMainCast: TextView = findViewById(R.id.item_mainCast)
                                val itemRating: TextView = findViewById(R.id.item_rating)

                                val image = response.getString("backdrop_path")
                                var director = ""

                                val jsonArrayCrew = responseCredits.getJSONArray("crew")
                                for (i in 0 until jsonArrayCrew.length()) {
                                    if(jsonArrayCrew.getJSONObject(i).getString("job") == "Director") {
                                        director =
                                            jsonArrayCrew.getJSONObject(i).getString("original_name")
                                        break
                                    }
                                }

                                val jsonArrayCast = responseCredits.getJSONArray("cast")
                                val mainCast = jsonArrayCast.getJSONObject(0).getString("original_name") + "\n" +
                                        jsonArrayCast.getJSONObject(1).getString("original_name") + "\n" +
                                        jsonArrayCast.getJSONObject(2).getString("original_name")

                                Picasso.get().load("$imageUrl$image").into(itemImage)
                                itemTitle.text = response.getString("original_title")
                                itemInfo.text = response.getString("release_date").take(4) + " - " + director
                                itemMainCast.text = mainCast
                                itemRating.text = (response.getDouble("vote_average") * 10).toInt().toString() + "%"

                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }, { error -> error.printStackTrace()})
                    queue?.add(requestCredits)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, { error -> error.printStackTrace() })
        queue?.add(request)
    }
}