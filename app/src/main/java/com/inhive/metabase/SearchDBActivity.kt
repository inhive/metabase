package com.inhive.metabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException

class SearchDBActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null

    private var queue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_dbactivity)

        val searchBtn: Button = findViewById(R.id.buttonSearch)
        val titleTextView: TextView = findViewById(R.id.editTextTitle)

        recyclerView = findViewById(R.id.RecycleViewResultItems)

        queue = Volley.newRequestQueue(this)

        searchBtn.setOnClickListener {
            if (titleTextView.text.isNullOrBlank()){
                //TODO: Rückmeldung, dass Eingabe nicht leer sein darf (EGT-87)
            } else {
                jsonParse(titleTextView.text.toString())
            }
        }
    }

    private val apiKey: String = "api_key=3fec234a19280c85911ee86c1098689f"
    private var url: String = "https://api.themoviedb.org/3/search/movie?$apiKey&query="
    private var imageUrl: String = "https://image.tmdb.org/t/p/original"

    private fun jsonParse(query: String) {
        val fullUrl = url + query
        val request =
            JsonObjectRequest(Request.Method.GET, fullUrl, null, { response ->
                try {
                    val itemTitles: MutableList<String> = mutableListOf()
                    val itemReleaseDates: MutableList<String> = mutableListOf()
                    val itemRatings: MutableList<String> = mutableListOf()
                    val itemPoster: MutableList<String> = mutableListOf()
                    val itemIds: MutableList<Int> = mutableListOf()

                    val jsonArray = response.getJSONArray("results")
                    for (i in 0 until jsonArray.length()) {
                        val movie = jsonArray.getJSONObject(i)

                        itemTitles.add(movie.getString("original_title"))
                        itemReleaseDates.add(movie.getString("release_date").take(4))
                        itemRatings.add((movie.getDouble("vote_average") * 10).toInt().toString() + "%")
                        itemPoster.add(imageUrl + movie.getString("poster_path"))
                        //TODO: If empty check if backdrop picture exists. If not show sample picture (EGT-80)
                        itemIds.add(movie.getInt("id"))
                    }
                    layoutManager = LinearLayoutManager(this)
                    recyclerView.layoutManager = layoutManager
                    recyclerView.adapter = RecyclerAdapter(itemTitles,itemReleaseDates,itemRatings, itemPoster, itemIds)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, { error -> error.printStackTrace() })
        queue?.add(request)
    }
}